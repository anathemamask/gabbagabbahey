
       _____          ____  ____             _____          ____  ____          
      / ____|   /\   |  _ \|  _ \   /\      / ____|   /\   |  _ \|  _ \   /\    
     | |  __   /  \  | |_) | |_) | /  \    | |  __   /  \  | |_) | |_) | /  \   
     | | |_ | / /\ \ |  _ <|  _ < / /\ \   | | |_ | / /\ \ |  _ <|  _ < / /\ \  
     | |__| |/ ____ \| |_) | |_) / ____ \  | |__| |/ ____ \| |_) | |_) / ____ \ 
     _\_____/_/  __\_\____/|____/_/  __\_\ _\_____/_/__ _\_\____/|____/_/_   \_\
     \ \        / /  ____|     /\   / ____/ ____|  ____|  __ \__   __|  | |  | |
      \ \  /\  / /| |__       /  \ | |   | |    | |__  | |__) | | |     | |  | |
       \ \/  \/ / |  __|     / /\ \| |   | |    |  __| |  ___/  | |     | |  | |
        \  /\  /  | |____   / ____ \ |___| |____| |____| |      | |     | |__| |
         \/  \/   |______| /_/    \_\_____\_____|______|_|      |_|      \____/ 

## One of us

Install using the foundry module browser with the module.json manifest
## Usage
if you need to remove this module once activated and you cannot restart the server, contact a foundry moderator and ask them to check the pinned messages in the moderator chatroom for the unlock command.

## What does it do?
This super simple module removes visibility for the Return to Setup, Module Configuration, and Player Configuration menus on foundry servers intended for use as a GM demo server.

## License
open source, open distribution, do whatever you want.